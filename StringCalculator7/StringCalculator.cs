﻿using System;
using System.Collections.Generic;

namespace StringCalculatorV7
{
    public class StringCalculator
    {
        private char forwardSlash =  '/';
        private char newLine =  '\n';
        private char comma =  ',';
        private string doubleSlashLeftSquareBracket = "//[";
        private string doubleSlash = "//";
        private char leftSquareBracket = '[';
        private char rightSquareBracket = ']';
       
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var delimiterList = GetDelimiters(numbers);
            var numbersArray = GetNumbersArray(numbers, delimiterList);
            var convertedNumbList = GetConvertedNumbersList(numbersArray);

            CheckForNegativeNumbers(convertedNumbList);

            var numbersLessThanThousand = GetNumbersLessThanThousand(convertedNumbList);
            var total = GetTotal(numbersLessThanThousand);

            return total;
        }
        private List<string> GetDelimiters(string numbers)
        {
            var delimitersList = new List<string>(new string[] { comma.ToString(), newLine.ToString() });

            if (numbers.Contains(doubleSlashLeftSquareBracket.ToString()))
            {
                delimitersList.Clear();

                var delimiters = numbers.Substring(numbers.IndexOf(leftSquareBracket), numbers.LastIndexOf(rightSquareBracket) - 1);

                foreach (var delimiter in delimiters.Split(leftSquareBracket,rightSquareBracket))
                {
                    delimitersList.Add(delimiter);
                }
            }

            return delimitersList;
        }

        private string[] GetNumbersArray(string numbers, List<string> delimitersList)
        {
            if (numbers.Contains(doubleSlash))
            {
                numbers = string.Concat(numbers.Split(forwardSlash));
                var delimiters = numbers.Substring(0, numbers.IndexOf(newLine));

                delimitersList.Add(delimiters);

                numbers = string.Concat(numbers.Split(forwardSlash, newLine));
            }

            if (numbers.Contains(doubleSlashLeftSquareBracket))
            {
                numbers = string.Concat(numbers.Split(forwardSlash,newLine, rightSquareBracket, leftSquareBracket));
            }

            return numbers.Split(delimitersList.ToArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        private List<int> GetConvertedNumbersList(string[] numbArray)
        {
            var numberList = new List<int>();
            var invalidDelimiters = string.Empty;

            foreach (var number in numbArray)
            {
                try
                {
                    numberList.Add(int.Parse(number));
                }
                catch 
                { 
                    throw new Exception("Invalid Delimiter !."); 
                }
            }
            
            return numberList;
        }

        private void CheckForNegativeNumbers(List<int> numberlist)
        {
            var negativeNumbers = string.Empty;

            foreach (var number in numberlist)
            {
                if (number < 0)
                {
                    negativeNumbers = string.Join(" ", negativeNumbers, number);
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                throw new Exception("Negatives not Allowed." + negativeNumbers);
            }
        }

        private List<int> GetNumbersLessThanThousand(List<int> convertedNumbersList)
        {
            var numberList = new List<int>();

            foreach (var number in convertedNumbersList)
            {
                if (number <= 1000)
                {
                    numberList.Add(number);
                }
            }

            return numberList;
        }

        private int GetTotal(List<int> numbersList)
        {
            var sum = 0;
            numbersList.ForEach(l => sum += l);
            return sum;
        }
    }
}
