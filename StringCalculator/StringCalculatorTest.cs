﻿using StringCalculatorV7;
using NUnit.Framework;
using System;

namespace StringCalculatorTest
{
    [TestFixture]
    public class StringCalculatorTest
    {
        private StringCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new StringCalculator();
        }

        [Test]
        public void GivenEmptyString_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 0;

            //Act
            var actual = _calculator.Add("");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenOneNumber_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 1;

            //Act
            var actual = _calculator.Add("1");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenTwoNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actaul = _calculator.Add("1,2");

            //Assert
            Assert.AreEqual(expected, actaul);
        }

        [Test]
        public void GivenUnlimitedNumbers_WhenAdding_ThenReturn_Sum()
        {
            //Arrange
            var expected = 20;

            //Act
            var actaul = _calculator.Add("10,1,1,1,2,5");

            //Assert
            Assert.AreEqual(expected, actaul);
        }

        [Test]
        public void GivenNewLineAsDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actaul = _calculator.Add("1\n2,3");

            //Assert
            Assert.AreEqual(expected, actaul);
        }

        [Test]
        public void GivenCustomDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("//;\n1;2");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenNegativeNumbers_Adding_ThenThrowExpection()
        {
            //Arrange
            var expected = "Negatives not Allowed. -3 -4";

            //Act
            var actual = Assert.Throws<Exception>(() => _calculator.Add("//;\n2;-3;-4"));

            //Assert
            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GivenNumbers_WhenAdding_ThenIgnoreNumbersGreaterThousand()
        {
            //Arrange
            var expected = 3;

            //Act
            var actaul = _calculator.Add("//;\n2;1001;1");

            //Assert
            Assert.AreEqual(expected, actaul);
        }

        [Test]
        public void GivenAnyDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("//***\n1***2***3");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenMultipleDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("//[*][%]\n1*2%3");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenInvalidDelimiter_WhenAdding_ThenThrowException()
        {
            //Arrange
            var expected = "Invalid Delimiter !.";

            //Act
            var exception = Assert.Throws<Exception>(() => _calculator.Add("//[*][%]\n1*2$3"));

            //Assert
            Assert.AreEqual(expected, exception.Message);
        }

        [Test]
        public void GivenUnlimitedMultipleDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actaul = _calculator.Add("//[$$*][##%&]\n1$$*2##%&3");

            //Assert
            Assert.AreEqual(expected, actaul);
        }
    }
}
